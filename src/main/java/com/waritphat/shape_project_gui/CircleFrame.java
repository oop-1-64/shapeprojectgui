/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.shape_project_gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author domem
 */
public class CircleFrame extends JFrame {

    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalArea;
    JLabel lblResult;

    public CircleFrame() {
        super("Circle");
        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblRadius = new JLabel("Radius: ", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        this.add(lblRadius);

        txtRadius = new JTextField();
        txtRadius.setSize(70, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);

        btnCalArea = new JButton("Calculate");
        btnCalArea.setSize(100, 30);
        btnCalArea.setLocation(60, 40);
        this.add(btnCalArea);

        lblResult = new JLabel("Circle: Radius = ?? Area = ?? Perimeter = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 80);
        this.add(lblResult);

        btnCalArea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strRadius = txtRadius.getText();
                double radius = Double.parseDouble(strRadius);
                Circle circle = new Circle(radius);
                lblResult.setText("Circle: Radius = " + String.format("%.2f", radius)
                        + " Area = " + String.format("%.2f", circle.calArea())
                        + " Perimeter = " + String.format("%.2f", circle.calPerimeter()));
                } catch (Exception ex){
                    JOptionPane.showMessageDialog(CircleFrame.this, "Error: Please input number!"
                            , "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        CircleFrame frame = new CircleFrame();
        frame.setVisible(true);
    }

}
